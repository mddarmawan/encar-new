<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('level');

Route::get('/home', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('kendaraan', function () {
    return view('modules.kendaraan.data');
});

Route::get('aksesoris', function () {
    return view('modules.aksesoris.data');
});

Route::get('asuransi', function () {
    return view('modules.asuransi.data');
});

/*
|--------------------------------------------------------------------------
| CRUD Leasing
|--------------------------------------------------------------------------
*/
Route::get('/kontak', 'KontakController@index');
Route::delete('/kontak/{id}', 'KontakController@destroy');
Route::post('/kontak', 'KontakController@store');
Route::put('/kontak', 'KontakController@update');
Route::get('/kontak/{id}', 'KontakController@show');

/*
|--------------------------------------------------------------------------
| CRUD Leasing
|--------------------------------------------------------------------------
*/
Route::get('/leasing', 'LeasingController@index');
Route::delete('/leasing/{id}', 'LeasingController@destroy');
Route::post('/leasing', 'LeasingController@store');
Route::put('/leasing', 'LeasingController@update');
Route::get('/leasing/{id}', 'LeasingController@show');

/*
|--------------------------------------------------------------------------
| CRUD Aksesoris
|--------------------------------------------------------------------------
*/
Route::delete('/aksesoris/{id}', 'AksesorisController@destroy');
Route::post('/aksesoris', 'AksesorisController@store');
Route::put('/aksesoris', 'AksesorisController@update');
Route::get('/aksesoris/{id}', 'AksesorisController@show');

/*
|--------------------------------------------------------------------------
| CRUD Asuransi
|--------------------------------------------------------------------------
*/
Route::get('api/asuransi', ['middleware' => 'cors','uses'=>'AsuransiController@index']);

Route::get('api/kendaraan/variant', ['middleware' => 'cors','uses'=>'KendaraanController@variant']);
Route::get('api/kendaraan/type', ['middleware' => 'cors','uses'=>'KendaraanController@type']);

Route::get('api/leasing', ['middleware' => 'cors','uses'=>'LeasingController@index']);

/*
|--------------------------------------------------------------------------
| ALL API's
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

