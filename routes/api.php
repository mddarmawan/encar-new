<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
|--------------------------------------------------------------------------
| API Kontak
|--------------------------------------------------------------------------
*/
Route::get('kontak/deleted',		'Api\ApiKontakController@deleted');
Route::get('kontak/inactive',		'Api\ApiKontakController@inactive');
Route::get('kontak/{type}/{id}',	'Api\ApiKontakController@order');
Route::get('kontak/{id}',			'Api\ApiKontakController@show');
Route::get('kontak',				'Api\ApiKontakController@data');

/*
|--------------------------------------------------------------------------
| API Kontak Tipe
|--------------------------------------------------------------------------
*/
Route::get('kontakTipe/{id}',		'Api\ApiKontakTipeController@show');
Route::get('kontakTipe',			'Api\ApiKontakTipeController@data');

/*
|--------------------------------------------------------------------------
| API Kontak Kategori
|--------------------------------------------------------------------------
*/
Route::get('kontakKategori/{id}',	'Api\ApiKontakKategoriController@show');
Route::get('kontakKategori',		'Api\ApiKontakKategoriController@data');

/*
|--------------------------------------------------------------------------
| API Aksesoris
|--------------------------------------------------------------------------
*/
// Route::get('api/aksesoris', ['middleware' => 'cors','uses'=>'AksesorisController@index']);
// Route::get('api/aksesoris/type', ['middleware' => 'cors','uses'=>'AksesorisController@type']);
// Route::get('api/aksesoris/vendor', ['middleware' => 'cors','uses'=>'AksesorisController@vendorAks']);
Route::get('aksesoris/deleted',		'Api\ApiAksesorisController@deleted');
Route::get('aksesoris/inactive',	'Api\ApiAksesorisController@inactive');
Route::get('aksesoris/{id}',		'Api\ApiAksesorisController@show');
Route::get('aksesoris',				'Api\ApiAksesorisController@data');