<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class KendaraanController extends Controller
{
    
    function variant(){
		$variant = DB::table('tb_variant')
                ->join('tb_type', 'tb_variant.variant_type', '=', 'tb_type.type_id')
                ->where("variant_hapus", 0)
                ->get();

        $result = $variant->filter(function ($variant) {
            return 
                (!request("variant_serial") || strrpos(strtolower($variant->variant_serial), strtolower(request("variant_serial"))) > -1) && 
                (!request("variant_nama") || strrpos(strtolower($variant->variant_nama), strtolower(request("variant_nama"))) > -1)&& 
                (!request("variant_status") || strrpos(strtolower($variant->variant_status), strtolower(request("variant_status"))) > -1)&& 
                (!request("variant_type") || strrpos(strtolower($variant->variant_type), strtolower(request("variant_type"))) > -1);

                 });
        $data = array();
        foreach($result as $d){
            $item = array();
            $item['variant_id'] = $d->variant_id;
            $item['variant_serial'] = $d->variant_serial;
            $item['variant_nama'] = $d->variant_nama;
            $item['variant_type'] = $d->variant_type;
            $item['variant_ket'] = $d->variant_ket;
            $item['variant_status'] = $d->variant_status;
            $item['type_poin'] = $d->type_poin;
            $item['edit'] = "
                <form action='/kendaraan/variant/".$d->variant_id."' method='post'>
                    <input type='hidden' name='_token' value='".csrf_token()."'>
                    <input type='hidden' name='_method' value='delete'>
                    <input type='submit' name='submit' value='submit' style='display: none;'>
                    <a href='#' class='orange-text' onclick='javascript:edit(".$d->variant_id.");'>
                        <span class='s7-pen'></span> Update
                    </a> ||
                    <span class='red-text s7-trash'></span>
                    <input style='background-color: transparent; border: none; cursor: pointer;' type='submit' class='red-text' onclick='return confirm(\"Anda yakin ingin menghapus?\"); this.submit();' value='Delete'>
                        
                    </input>
                </form> ";
            array_push($data, $item);
        }
        return json_encode($data);
	}


	function type($id = NULL){
        if ($id === NULL) {
            $data = DB::table('tb_type')
                    ->orderBy("type_nama","ASC")
                    ->where("type_hapus", 0)
                    ->get();
            $result = $data->filter(function ($data) {
                return 
                    (!request("type_nama") || strrpos(strtolower($data->type_nama), strtolower(request("type_nama"))) > -1) &&
                    (!request("type_poin") || strrpos(strtolower($data->type_point), strtolower(request("type_poin"))) > -1) &&
                    (!request("type_status") || strrpos(strtolower($data->type_status), strtolower(request("type_status")))) ;
                });

            return json_encode($result);
        } else {
            $id = str_replace("%20", " ", $id);
            $data = DB::table('tb_type') 
                    -> where("type_nama", $id)
                    ->get();
            $result = $data->filter(function ($data) {
                return 
                    (!request("type_nama") || strrpos(strtolower($data->type_nama), strtolower(request("type_nama"))) > -1) &&
                     (!request("type_poin") || strrpos(strtolower($data->type_point), strtolower(request("type_poin"))) > -1)&&
                     (!request("type_status") || strrpos(strtolower($data->type_status), strtolower(request("type_status"))) > -1) ;
            });

            return json_encode($result);
        }
    }
}