<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AksesorisController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('modules.aksesoris.data');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$params = array(
			"aksesoris_vendor" => $request->aksesoris_vendor,
			"aksesoris_kendaraan" => $request->aksesoris_kendaraan,
			"aksesoris_kode" => $request->aksesoris_kode,
			"aksesoris_nama" => $request->aksesoris_nama,
			"aksesoris_harga" => $request->aksesoris_harga
		);

		$data = DB::table("tb_aksesoris")->insert($params);

		if ($data) {
			return redirect('/aksesoris')->with('message', 'Berhasil menambah.');
		} else {
			return redirect('/aksesoris')->with('message', 'Gagal menambah.');
		}
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$params = array(
			"aksesoris_id" => $request->aksesoris_id,
			"aksesoris_vendor" => $request->aksesoris_vendor,
			"aksesoris_kendaraan" => $request->aksesoris_kendaraan,
			"aksesoris_kode" => $request->aksesoris_kode,
			"aksesoris_nama" => $request->aksesoris_nama,
			"aksesoris_harga" => $request->aksesoris_harga
		);

		$data = DB::table("tb_aksesoris")->where("aksesoris_id", $id)->update($params);

		return redirect('/aksesoris')->with('message', 'Berhasil mengedit.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$data = DB::table("tb_aksesoris")->where("aksesoris_id", $id)->delete();

		if ($data) {
			return redirect('/aksesoris')->with('message', 'Berhasil menghapus.');
		} else {
			return redirect('/aksesoris')->with('message', 'Gagal menghapus.');
		}
	}
}
