<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiKontakController extends Controller
{
	private $table;
	private $column;

	/**
	 * Menentukan Table dan Kolom yang akan di gunakan selanjutnya.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->table = "tb_kontak";
		$this->column = "kontak";
	}
	
	/**
	 * Menampilkan Data yang terpilih.
	 *
	 * @param int $id
	 * @return void
	 */
	public function show($id)
	{
		$where = array(
			$this->column . "_id" => $id
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan Data yang berdasarkan yang di inginkan.
	 *
	 * @param int $id
	 * @return void
	 */
	public function order($type, $id)
	{
		$column = $this->column . "_" . $type;

		$where = array(
			$column => $id
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan Data yang tidak di aktifkan.
	 *
	 * @return void
	 */
	public function inactive()
	{
		$where = array(
			$this->column . "_status" => "0"
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan Data yang masuk ke Recycle Bin.
	 *
	 * @return void
	 */
	public function deleted()
	{
		$where = array(
			$this->column . "_hapus" => "1"
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan seluruh Data.
	 *
	 * @param array $where
	 * @return void
	 */
	public function data($where = NULL)
	{
		if (!empty($where)) {
			$where = $where;
			$editing = TRUE;
		} else {
			$where = array();
			$editing = FALSE;
		}

		if (!isset($where[$this->column . "_status"])) {
			$where[$this->column . "_status"] = "1";
		}

		$data = DB::table($this->table)
				->where($where)
				->get();

		$result = array();
		foreach($data as $r){
			$kontak_tipe = DB::table("tb_kontak_tipe")->where("kontak_tipe_id", $r->kontak_tipe)->first();
			$kontak_kategori = DB::table("tb_kontak_kategori")->where("kontak_kategori_id", $r->kontak_kategori)->first();

			$item = array();
			$item['kontak_id'] = $r->kontak_id;
			$item['kontak_tipe'] = $r->kontak_tipe;
			$item['kontak_kategori'] = $r->kontak_kategori;
			$item['kontak_tipe_nama'] = ucwords(strtolower($kontak_tipe->kontak_tipe_nama));
			$item['kontak_tipe_kode'] = $kontak_kategori->kontak_kategori_tipe;
			$item['kontak_kategori_nama'] = ucwords(strtolower($kontak_kategori->kontak_kategori_nama));
			$item['kontak_nama'] = $r->kontak_nama;
			$item['kontak_nick'] = $r->kontak_nick;
			$item['kontak_alamat'] = $r->kontak_alamat;
			$item['kontak_kota'] = $r->kontak_kota;
			$item['kontak_pos'] = $r->kontak_pos;
			$item['kontak_telp'] = $r->kontak_telp;
			$item['kontak_email'] = $r->kontak_email;
			$item['kontak_hapus'] = $r->kontak_hapus;
			$item['kontak_status'] = $r->kontak_status;
			$item['kontak_tgl'] = date_format(date_create($r->created_at),"d/m/Y");

			if (!$editing) {
				$item['edit'] = "
					<form action='/kontak/".$r->kontak_id."' method='post'>
						<input type='hidden' name='_token' value='".csrf_token()."'>
						<input type='hidden' name='_method' value='delete'>
						<input type='submit' name='submit' value='submit' style='display: none;'>
						<a href='#' class='orange-text' onclick='javascript:edit(\"".$r->kontak_id."\");'>
							<span class='s7-pen'></span> Update
						</a> ||
						<span class='red-text s7-trash'></span>
						<input style='background-color: transparent; border: none; cursor: pointer;' type='submit' class='red-text' onclick='return confirm(\"Anda yakin ingin menghapus?\"); this.submit();' value='Delete'>
							
						</input>
					</form>";
			}

			if((!request("kontak_tipe") || strrpos(strtolower($item['kontak_tipe']), strtolower(request("kontak_tipe"))) > -1) &&
				(!request("kontak_kategori") || strrpos(strtolower($item['kontak_kategori']), strtolower(request("kontak_kategori"))) > -1) &&
				(!request("kontak_nama") || strrpos(strtolower($item['kontak_nama']), strtolower(request("kontak_nama"))) > -1) &&
				(!request("kontak_nick") || strrpos(strtolower($item['kontak_nick']), strtolower(request("kontak_nick"))) > -1) &&
				(!request("kontak_alamat") || strrpos(strtolower($item['kontak_alamat']), strtolower(request("kontak_alamat"))) > -1) &&
				(!request("kontak_kota") || strrpos(strtolower($item['kontak_kota']), strtolower(request("kontak_kota"))) > -1) &&
				(!request("kontak_pos") || strrpos(strtolower($item['kontak_pos']), strtolower(request("kontak_pos"))) > -1) &&
				(!request("kontak_telp") || strrpos(strtolower($item['kontak_telp']), strtolower(request("kontak_telp"))) > -1) &&
				(!request("kontak_email") || strrpos(strtolower($item['kontak_email']), strtolower(request("kontak_email"))) > -1))
			{
				$tgl = strtotime(str_replace("/","-",$item['kontak_tgl']));
				if (request("filter_awal") && request("filter_akhir")){
					$filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
					$filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
					if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
						array_push($result, $item);                     
					}
				}else if (request("filter_awal")){
					$filter_awal = strtotime(request("filter_awal"));
					if ($filter_awal<=$tgl){
						array_push($result, $item);                     
					}
				}else if (request("filter_akhir")){
					$filter_akhir = strtotime(request("filter_akhir"));
					if ($filter_akhir>=$tgl){
						array_push($result, $item);                     
					}
				}else{
					array_push($result, $item);
				}
			}
		}

		return json_encode($result);
	}
}
