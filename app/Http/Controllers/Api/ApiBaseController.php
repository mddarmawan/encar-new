<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiBaseController extends Controller
{
	private $table;
	private $column;

	/**
	 * Menentukan Table dan Kolom yang akan di gunakan selanjutnya.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->table = "tb_ base";
		$this->column = "";
	}
	
	/**
	 * Menampilkan Data yang terpilih.
	 *
	 * @param int $id
	 * @return void
	 */
	public function show($id)
	{
		$where = array(
			$this->column . "_id" => $id
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan Data yang tidak di aktifkan.
	 *
	 * @return void
	 */
	public function inactive()
	{
		$where = array(
			$this->column . "_status" => "0"
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan Data yang masuk ke Recycle Bin.
	 *
	 * @return void
	 */
	public function deleted()
	{
		$where = array(
			$this->column . "_hapus" => "1"
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan seluruh Data.
	 *
	 * @param array $where
	 * @return void
	 */
	public function data($where = NULL)
	{
		if (!empty($where)) {
			$where = $where;
		} else {
			$where = array();
		}

		if (!isset($where[$this->column . "_status"])) {
			$where[$this->column . "_status"] = "1";
		}

		$data = DB::table($this->table)
				->where($where)
				->get();

		$result = array();
		foreach($data as $r){
			$item = array();
			$item['base _'] = $r->base_id;
			$item['base _tgl'] = date_format(date_create($r->created_at),"d/m/Y");

			if((!request("base _") || strrpos(strtolower($item['base _']), strtolower(request("base _"))) > -1) &&
				(!request("base _") || strrpos(strtolower($item['base _']), strtolower(request("base _"))) > -1))
			{
				$tgl = strtotime(str_replace("/","-",$item['kontak_tgl']));
				if (request("filter_awal") && request("filter_akhir")){
					$filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
					$filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
					if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
						array_push($result, $item);                     
					}
				}else if (request("filter_awal")){
					$filter_awal = strtotime(request("filter_awal"));
					if ($filter_awal<=$tgl){
						array_push($result, $item);                     
					}
				}else if (request("filter_akhir")){
					$filter_akhir = strtotime(request("filter_akhir"));
					if ($filter_akhir>=$tgl){
						array_push($result, $item);                     
					}
				}else{
					array_push($result, $item);
				}
			}
		}

		return json_encode($result);
	}
}
