<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiAksesorisController extends Controller
{
	private $table;
	private $column;

	/**
	 * Menentukan Table dan Kolom yang akan di gunakan selanjutnya.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->table = "tb_aksesoris";
		$this->column = "aksesoris";
	}
	
	/**
	 * Menampilkan Data yang terpilih.
	 *
	 * @param int $id
	 * @return void
	 */
	public function show($id)
	{
		$where = array(
			$this->column . "_kode" => $id
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan Data yang tidak di aktifkan.
	 *
	 * @return void
	 */
	public function inactive()
	{
		$where = array(
			$this->column . "_status" => "0"
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan Data yang masuk ke Recycle Bin.
	 *
	 * @return void
	 */
	public function deleted()
	{
		$where = array(
			$this->column . "_hapus" => "1"
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan seluruh data Aksesoris.
	 *
	 * @param array $where
	 * @return void
	 */
	public function data($where = NULL)
	{
		$where = (!empty($where) ? $where : array());
		$data = DB::table($this->table)
				->where($where)
				->get();

		$result = array();
		foreach ($data as $r) {
			$item = array();
			$item['aksesoris_kode'] = $r->aksesoris_kode;
			$item['aksesoris_nama'] = $r->aksesoris_nama;
			$item['aksesoris_kendaraan'] = $r->aksesoris_kendaraan;
			$item['aksesoris_harga'] = number_format($r->aksesoris_harga,0,',','.');
			$item['aksesoris_vendor'] = $r->aksesoris_vendor;
			$item['aksesoris_tgl'] = date_format(date_create($r->created_at),"d/m/Y");

			if($r->aksesoris_status == 0){
				$item['aksesoris_status'] = "ALL";
			} else {
				$item['aksesoris_status'] = $r->aksesoris_status;
			}

			if ((!request("aksesoris_kode") || strrpos(strtolower($item['aksesoris_kode']), strtolower(request("aksesoris_kode"))) > -1) &&
				(!request("aksesoris_nama") || strrpos(strtolower($item['aksesoris_nama']), strtolower(request("aksesoris_nama"))) > -1) &&
				(!request("aksesoris_kendaraan") || strrpos(strtolower($item['aksesoris_kendaraan']), strtolower(request("aksesoris_kendaraan"))) > -1) &&
				(!request("aksesoris_harga") || strrpos(strtolower($item['aksesoris_harga']), strtolower(request("aksesoris_harga"))) > -1) &&
				(!request("aksesoris_vendor") || strrpos(strtolower($item['aksesoris_vendor']), strtolower(request("aksesoris_vendor"))) > -1) &&
				(!request("aksesoris_tgl") || strrpos(strtolower($item['aksesoris_tgl']), strtolower(request("aksesoris_tgl"))) > -1))
			{
				$tgl = strtotime(str_replace("/","-",$item['aksesoris_tgl']));
				if (request("filter_awal") && request("filter_akhir")) {
					$filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
					$filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
					if ($filter_awal<=$tgl && $filter_akhir>=$tgl) {
						array_push($result, $item);                     
					}
				} else if (request("filter_awal")) {
					$filter_awal = strtotime(request("filter_awal"));
					if ($filter_awal<=$tgl) {
						array_push($result, $item);                     
					}
				} else if (request("filter_akhir")) {
					$filter_akhir = strtotime(request("filter_akhir"));
					if ($filter_akhir>=$tgl) {
						array_push($result, $item);                     
					}
				} else {
					array_push($result, $item);
				}
			}
		}

		return json_encode($result);
	}
}
