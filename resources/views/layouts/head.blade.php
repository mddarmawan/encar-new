	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="{{ asset('assets') }}/img/favicon.html">
	<title>Encar Daihatsu</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/lib/stroke-7/style.css"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/lib/theme-switcher/theme-switcher.min.css"/>
	<link type="text/css" href="{{ asset('assets') }}/css/app.css" rel="stylesheet"> 
	<link type="text/css" href="{{ asset('assets') }}/css/custom.css" rel="stylesheet">
	<link type="text/css" href="{{ asset('assets') }}/lib/Avgrund-master/css/avgrund.css" rel="stylesheet">

	<link type="text/css" rel="stylesheet" href="{{ asset('bower_components') }}/jsgrid/dist/jsgrid.min.css" />
	<link type="text/css" rel="stylesheet" href="{{ asset('bower_components') }}/jsgrid/dist/jsgrid-theme.min.css" />