@extends('layouts.app')

@section('content')

	@if (!empty(Session::get('message')))
		<script>
			alert('{{ Session::get('message') }}')
		</script>
	@endif

	<div class="panel-header-encar">
		<h6 class="left">
			<small style="margin-bottom: 40px">Master Data</small>
			<br>
		</h6>
		 <h5>Aksesoris</h5>
	</div>


	<button class="tambah-flot" onclick="javascript:add();" class="btn btn-success">add</button>
	<div class="content_encar">
		<div class="main-content ">
			<div class="col-sm-12">
				<div class="panel panel-default panel-table">
					<div class="panel-body">
						<div id="dataAksesoris">

						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>

	<div id="add" class="avgrund-popup" >
		<form action="/aksesoris" method="post">
			{{ csrf_field() }}
			<input id="aksesoris_id" type="hidden" name="aksesoris_id">
			<div class="header-modal">
				<a href="#" class="close-modal" onclick="javascript:closeDialog();"><span class="s7-close"></span></a>
				<div class="title-modal">
					Tambah Aksesoris
					<br>
					<small> Nama Aksesoris</small>
				</div>
			</div>
			<div class="content-modal">
				<div class="row" style="margin: 0">
					 <div class="col-md-6">
						<div class="form-group col-md-10">
				          <label>Kode Aksesoris</label>
				          <input type="text" placeholder="" name="aksesoris_kode" class="form-control form-control-xs">
				        </div>
				        <div class="form-group col-md-10">
				          <label>Nama Aksesoris</label>
				          <input type="text" placeholder="" name="aksesoris_nama" class="form-control form-control-xs">
				        </div>
						<div class="form-group col-md-10">
				          <label>Kendaraan</label>
				          <select class="form-control custom-select small" id="type" name="aksesoris_kendaraan">
	                       
	                      </select>
				        </div>
				 	</div>
					<div class="col-md-6">
						<div class="form-group col-md-10">
				          <label>Harga</label>
				          <input type="number" placeholder="" name="aksesoris_harga" class="form-control form-control-xs">
						</div>
						<div class="form-group col-md-10">
				          <label>Vendor Aksesoris</label>
				           <select class="form-control custom-select small" id="vendorAks" name="aksesoris_vendor">
	                       
	                      </select>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-modal">
				<div style="float: right; margin-right: 30px" >
					<button type="submit" name="submit" class="simpan">Simpan</button>	
				</div>
			</div>
		</form>
	</div>

	<div id="edit" class="avgrund-popup" >
		<form action="/aksesoris" method="post">
			{{ csrf_field() }}
			<input type="hidden" name="_method" value="put">
			<input id="aksesoris_id" type="hidden" name="aksesoris_id">
			<div class="header-modal">
				<a href="#" class="close-modal" onclick="javascript:closeDialog();"><span class="s7-close"></span></a>
				<div class="title-modal">
					Edit Aksesoris
					<br>
				</div>
			</div>
			<div class="content-modal">
				<div class="row" style="margin: 0">
					 <div class="col-md-6">
						<div class="form-group col-md-10">
				          <label>Kode Aksesoris</label>
				          <input type="text" placeholder="" id="aksesoris_kode" name="aksesoris_kode" class="form-control form-control-xs">
				        </div>
				        <div class="form-group col-md-10">
				          <label>Nama</label>
				          <input type="text" placeholder="" id="aksesoris_nama" name="aksesoris_nama" class="form-control form-control-xs">
				        </div>
						<div class="form-group col-md-10">
				          <label>Kendaraan</label>
				          <select class="form-control custom-select small" id="type" name="aksesoris_kendaraan">
	                       
	                      </select>
				        </div>
				 	</div>
					<div class="col-md-6">
						<div class="form-group col-md-10">
				          <label>Harga</label>
				          <input type="number" placeholder="" id="aksesoris_harga" name="aksesoris_harga" class="form-control form-control-xs">
						</div>
						<div class="form-group col-md-10">
				          <label>Vendor Aksesoris</label>
				           <select class="form-control custom-select small" id="vendorAks" name="aksesoris_vendor">
	                       
	                      </select>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-modal">
				<div style="float: right; margin-right: 30px" >
					<button type="submit" name="submit" class="simpan">Simpan</button>		
				</div>
			</div>
		</form>
	</div>
	
	@section('scripts')

		<script type="text/javascript">
			function clearForm() {	
				$("#aksesoris_id").val('');
				$("#aksesoris_kode").val('');
				$("#aksesoris_nama").val('');
				$("#aksesoris_kendaraan").val('');
				$("#aksesoris_harga").val('');
				$("#aksesoris_vendor").val('');

			}

			function add() {
				Avgrund.show( "#add" );
				setVendor();
				setType();
			}

			function setType(){
				$.ajax({
				    type: "GET",
				    url: "{{url('api/aksesoris/type')}}"
				}).done(function(data) {
					$("#type").html("");
					jQuery.each(data, function(i, item) {
						$("#type").append("<option value='"+ item.type_id +"'>"+ item.type_nama +"</option>");
					});
				});
			}

			function setVendor(){
				$.ajax({
				    type: "GET",
				    url: "{{url('api/aksesoris/vendor')}}"
				}).done(function(data) {
					$("#vendorAks").html("");
					jQuery.each(data, function(i, item) {
						$("#vendorAks").append("<option value='"+ item.vendorAks_id +"'>"+ item.vendorAks_nama +"</option>");
					});
				});
			}

			function edit(id) {
				clearForm();

				$.ajax({
					type: "GET",
					url: "{{url('aksesoris')}}/" + id
				}).done(function(response){
					var data = JSON.parse(response);

					$("#aksesoris_id").val(data.aksesoris_id);
					$("#aksesoris_kode").val(data.aksesoris_kode);
					$("#aksesoris_nama").val(data.aksesoris_nama);
					$("#aksesoris_kendaraan").val(data.aksesoris_kendaraan);
					$("#aksesoris_harga").val(data.aksesoris_harga);
					$("#aksesoris_vendor").val(data.aksesoris_vendor);
				});

				Avgrund.show( "#edit" );
				setVendor();
				setType();
			}

			function closeDialog() {
				Avgrund.hide();
			}

			$(document).ready(function(){
				//initialize the javascript
				App.init();

				var db = {
					loadData: function(filter) {
						return $.ajax({
							type: "GET",
							url: "{{url('api/aksesoris')}}",
							data: filter
						});
					},
				};
				 
				$("#dataAksesoris").jsGrid({
					width: "100%",
					height: "445px",
					sorting: true,
					paging: true,
					filtering: true,
					autoload: true,
					
					deleteConfirm: "Anda yakin akan menghapus data ini?",
						 
					controller: db,
						 
					fields: [
							{ name: "aksesoris_kode", title:"Kode Aksesoris", type: "text", width: 100 },
							{ name: "aksesoris_nama", title:"Nama Aksesoris", type: "text", width: 130, validate: "required" },
							{ name: "aksesoris_kendaraan", title:"Kendaraan", type: "text", width: 130, validate: "required" },
							{ name: "aksesoris_harga", title:"Harga", type: "number", width: 100, validate: "required", align:"right" },
							{ name: "aksesoris_vendor", title:"Vendor", type: "text", width: 100, inserting: false, editing:false },
							{ name: "edit", title:"Action", type: "text", width: 80, validate: "required", align:"center" },
					]

				});
			});
		</script>

	@endsection

@endsection