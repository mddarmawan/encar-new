@extends('layouts.app')

@section('content')

	@if (!empty(Session::get('message')))
		<script>
			alert('{{ Session::get('message') }}')
		</script>
	@endif

	<div class="panel-header-encar">
		<h6 class="left">
			<small style="margin-bottom: 40px">Master Data</small>
			<br>
		</h6>
		 <h5>Asuransu</h5>
	</div>


	<button class="tambah-flot" onclick="javascript:add();" class="btn btn-success">add</button>
	<div class="content_encar">
		<div class="main-content ">
			<div class="col-sm-12">
				<div class="panel panel-default panel-table">
					<div class="panel-body">
						<div id="dataAsuransi">

						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>

	<div id="add" class="avgrund-popup" >
		<form action="/asuransi" method="post">
			{{ csrf_field() }}
			<input id="asuransi_id" type="hidden" name="asuransi_id">
			<div class="header-modal">
				<a href="#" class="close-modal" onclick="javascript:closeDialog();"><span class="s7-close"></span></a>
				<div class="title-modal">
					Tambah Asuransi
					<br>
				</div>
			</div>
			<div class="content-modal">
				<div class="row" style="margin: 0">
					 <div class="col-md-6">
						<div class="form-group col-md-10">
				          <label>Nama Asuransi</label>
				          <input type="text" placeholder="" name="asuransi_nama" class="form-control form-control-xs">
				        </div>
				        <div class="form-group col-md-10">
				          <label>Telp</label>
				          <input type="text" placeholder="" name="asuransi_telp" class="form-control form-control-xs">
				        </div>
						<div class="form-group col-md-10">
				          <label>Email</label>
				          <input type="text" placeholder="" name="asuransi_email" class="form-control form-control-xs">				        </div>
				 	</div>
					<div class="col-md-6">
						<div class="form-group col-md-10">
				          <label>Alamat</label>
							<textarea class="form-control" style="height: 80px" name="asuransi_alamat"></textarea>
						</div>
						<div class="form-group col-md-10">
				          <label>Keterangan</label>
				          <input type="text" placeholder="" name="asuransi_keterangan" class="form-control form-control-xs">
						</div>
					</div>
				</div>
			</div>
			<div class="footer-modal">
				<div style="float: right; margin-right: 30px" >
					<button type="submit" name="submit" class="simpan">Simpan</button>	
				</div>
			</div>
		</form>
	</div>

	<div id="edit" class="avgrund-popup" >
		<form action="/asuransi" method="post">
			{{ csrf_field() }}
			<input type="hidden" name="_method" value="put">
			<input id="asuransi_id" type="hidden" name="asuransi_id">
			<div class="header-modal">
				<a href="#" class="close-modal" onclick="javascript:closeDialog();"><span class="s7-close"></span></a>
				<div class="title-modal">
					Edit Aksesoris
					<br>
				</div>
			</div>
			<div class="content-modal">
				<div class="row" style="margin: 0">
					 <div class="col-md-6">
						<div class="form-group col-md-10">
				          <label>Nama Asuransi</label>
				          <input type="text" placeholder="" id="asuransi_nama" name="asuransi_nama" class="form-control form-control-xs">
				        </div>
				        <div class="form-group col-md-10">
				          <label>Telp</label>
				          <input type="text" placeholder="" id="asuransi_telp" name="asuransi_telp" class="form-control form-control-xs">
				        </div>
						<div class="form-group col-md-10">
				          <label>Email</label>
				          <input type="text" placeholder="" id="asuransi_email" name="asuransi_email" class="form-control form-control-xs">				        </div>
				 	</div>
					<div class="col-md-6">
						<div class="form-group col-md-10">
				          <label>Alamat</label>
							<textarea class="form-control" style="height: 80px" id="asuransi_alamat" name="asuransi_alamat"></textarea>
						</div>
						<div class="form-group col-md-10">
				          <label>Keterangan</label>
				          <input type="text" placeholder="" id="asuransi_keterangan" name="asuransi_keterangan" class="form-control form-control-xs">
						</div>
					</div>
				</div>
			</div>
			<div class="footer-modal">
				<div style="float: right; margin-right: 30px" >
					<button type="submit" name="submit" class="simpan">Simpan</button>		
				</div>
			</div>
		</form>
	</div>
	
	@section('scripts')

		<script type="text/javascript">
			function clearForm() {	
				$("#asuransi_id").val('');
				$("#asuransi_nama").val('');
				$("#asuransi_telp").val('');
				$("#asuransi_email").val('');
				$("#asuransi_alamat").val('');
				$("#asuransi_keterangan").val('');

				setType();
			}

			function add() {
				Avgrund.show( "#add" );
			}

			function edit(id) {
				clearForm();

				$.ajax({
					type: "GET",
					url: "{{url('asuransi')}}/" + id
				}).done(function(response){
					var data = JSON.parse(response);

					$("#asuransi_id").val(data.asuransi_id);
					$("#asuransi_nama").val(data.asuransi_nama);
					$("#asuransi_telp").val(data.asuransi_telp);
					$("#asuransi_email").val(data.asuransi_email);
					$("#asuransi_alamat").val(data.asuransi_alamat);
					$("#asuransi_keterangan").val(data.asuransi_keterangan);
				});

				Avgrund.show( "#edit" );
			}

			function closeDialog() {
				Avgrund.hide();
			}

			$(document).ready(function(){
				//initialize the javascript
				App.init();

				var db = {
					loadData: function(filter) {
						return $.ajax({
							type: "GET",
							url: "{{url('api/asuransi')}}",
							data: filter
						});
					},
				};
				 
				$("#dataAsuransi").jsGrid({
					width: "100%",
					height: "445px",
					sorting: true,
					paging: true,
					filtering: true,
					autoload: true,
					
					deleteConfirm: "Anda yakin akan menghapus data ini?",
						 
					controller: db,
						 
					fields: [
							{ name: "asuransi_nama", title:"Kode Aksesoris", type: "text", width: 100 },
							{ name: "asuransi_alamat", title:"Nama Aksesoris", type: "text", width: 130, validate: "required" },
							{ name: "asuransi_email", title:"Kendaraan", type: "text", width: 130, validate: "required" },
							{ name: "asuransi_telp", title:"Harga", type: "number", width: 100, validate: "required", align:"right" },
							{ name: "asuransi_keterangan", title:"Vendor", type: "text", width: 100, inserting: false, editing:false },
							{ name: "edit", title:"Action", type: "text", width: 80, validate: "required", align:"center" },
					]

				});
			});
		</script>

	@endsection

@endsection