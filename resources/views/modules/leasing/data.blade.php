@extends('layouts.app')

@section('content')

	@if (!empty(Session::get('message')))
		<script>
			alert('{{ Session::get('message') }}')
		</script>
	@endif

	<div class="panel-header-encar">
		<h6 class="left">
			<small style="margin-bottom: 40px">Master Data</small>
			<br>
		</h6>
		 <h5>Leasing</h5>
	</div>


	<button class="tambah-flot" onclick="javascript:add();" class="btn btn-success">add</button>
	<div class="content_encar">
		<div class="main-content ">
			<div class="col-sm-12">
				<div class="panel panel-default panel-table">
					<div class="panel-body">
						<div id="dataLeasing">

						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>

	<div id="add" class="avgrund-popup" >
		<form action="/leasing" method="post">
			{{ csrf_field() }}
			<input id="leasing_id" type="hidden" name="leasing_id">
			<div class="header-modal">
				<a href="#" class="close-modal" onclick="javascript:closeDialog();"><span class="s7-close"></span></a>
				<div class="title-modal">
					Tambah Leasing
					<br>
					<small> Nama Leasing</small>
				</div>
			</div>
			<div class="content-modal">
				<div class="row" style="margin: 0">
					 <div class="col-md-6">
						<div class="form-group col-md-10">
				          <label>Nama Leasing</label>
				          <input type="text" placeholder="" name="leasing_nama" class="form-control form-control-xs">
				        </div>
				        <div class="form-group col-md-10">
				          <label>Nick</label>
				          <input type="text" placeholder="" name="leasing_nick" class="form-control form-control-xs">
				        </div>
						<div class="form-group col-md-10">
				          <label>No. Telp</label>
				          <input type="text" placeholder="" name="leasing_telp" class="form-control form-control-xs">
				        </div>
				 	</div>
					<div class="col-md-6">
						<div class="form-group col-md-10">
				          <label>Alamat</label>
							<textarea class="form-control" style="height: 80px" name="leasing_alamat"></textarea>
						</div>
						<div class="form-group col-md-10">
				          <label>Kota</label>
				          <input type="text" placeholder="" name="leasing_kota" class="form-control form-control-xs">
						</div>
					</div>
				</div>
			</div>
			<div class="footer-modal">
				<div style="float: right; margin-right: 30px" >
					<button type="submit" name="submit" class="simpan">Simpan</button>	
				</div>
			</div>
		</form>
	</div>

	<div id="edit" class="avgrund-popup" >
		<form action="/leasing" method="post">
			{{ csrf_field() }}
			<input type="hidden" name="_method" value="put">
			<input id="leasing_id" type="hidden" name="leasing_id">
			<div class="header-modal">
				<a href="#" class="close-modal" onclick="javascript:closeDialog();"><span class="s7-close"></span></a>
				<div class="title-modal">
					Edit Leasing
					<br>
					<small> Nama Leasing</small>
				</div>
			</div>
			<div class="content-modal">
				<div class="row" style="margin: 0">
					 <div class="col-md-6">
						<div class="form-group col-md-10">
				          <label>Nama Leasing</label>
				          <input type="text" placeholder="" id="leasing_nama" name="leasing_nama" class="form-control form-control-xs">
				        </div>
				        <div class="form-group col-md-10">
				          <label>Nick</label>
				          <input type="text" placeholder="" id="leasing_nick" name="leasing_nick" class="form-control form-control-xs">
				        </div>
						<div class="form-group col-md-10">
				          <label>No. Telp</label>
				          <input type="text" placeholder="" id="leasing_telp" name="leasing_telp" class="form-control form-control-xs">
				        </div>
				 	</div>
					<div class="col-md-6">
						<div class="form-group col-md-10">
				          <label>Alamat</label>
							<textarea class="form-control" id="leasing_alamat" name="leasing_alamat"></textarea>
						</div>
						<div class="form-group col-md-10">
				          <label>Kota</label>
				          <input type="text" placeholder="" id="leasing_kota" name="leasing_kota" class="form-control form-control-xs">
						</div>
					</div>
				</div>
			</div>
			<div class="footer-modal">
				<div style="float: right; margin-right: 30px" >
					<button type="submit" name="submit" class="simpan">Simpan</button>
				</div>
			</div>
		</form>
	</div>
	
	@section('scripts')

		<script type="text/javascript">
			function clearForm() {	
				$("#leasing_id").val('');
				$("#leasing_nama").val('');
				$("#leasing_nick").val('');
				$("#leasing_telp").val('');
				$("#leasing_alamat").val('');
				$("#leasing_kota").val('');
			}

			function add() {
				Avgrund.show( "#add" );
			}

			function edit(id) {
				clearForm();

				$.ajax({
					type: "GET",
					url: "{{url('leasing')}}/" + id
				}).done(function(response){
					var data = JSON.parse(response);
					
					$("#leasing_id").val(data.leasing_id);
					$("#leasing_nama").val(data.leasing_nama);
					$("#leasing_nick").val(data.leasing_nick);
					$("#leasing_telp").val(data.leasing_telp);
					$("#leasing_alamat").val(data.leasing_alamat);
					$("#leasing_kota").val(data.leasing_kota);
				});

				Avgrund.show( "#edit" );
			}

			function closeDialog() {
				Avgrund.hide( "#edit" );
			}

			$(document).ready(function(){
				//initialize the javascript
				App.init();

				var db = {
					loadData: function(filter) {
						return $.ajax({
							type: "GET",
							url: "{{url('api/leasing')}}",
							data: filter
						});
					},
				};
				 
				$("#dataLeasing").jsGrid({
					width: "100%",
					height: "445px",
					sorting: true,
					paging: true,
					filtering: true,
					autoload: true,
					
					deleteConfirm: "Anda yakin akan menghapus data ini?",
						 
					controller: db,
						 
					fields: [
							{ name: "leasing_nama", title:"Nama Leasing", type: "text", width: 100 },
							{ name: "leasing_nick", title:"Nick Leasing", type: "text", width: 130, validate: "required" },
							{ name: "leasing_alamat", title:"Alamat", type: "text", width: 170, validate: "required" },
							{ name: "leasing_telp", title:"Telepon", type: "text", width: 170, validate: "required" },
							{ name: "leasing_kota", title:"Kota", type: "text", width: 40, inserting: false, editing:false },
							{ name: "edit", title:"Action", type: "text", width: 80, validate: "required", align:"center" },
					]

				});
			});
		</script>

	@endsection

@endsection